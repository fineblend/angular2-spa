import { TestBed, async, inject } from '@angular/core/testing';

import { AgendaGuard } from './agenda.guard';

describe('AgendaGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AgendaGuard]
    });
  });

  it('should ...', inject([AgendaGuard], (guard: AgendaGuard) => {
    expect(guard).toBeTruthy();
  }));
  it('should block users who are not logged in from accessing agenda');
});
