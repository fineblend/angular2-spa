import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  transform(value: any): any {
    let time;
    if ( (value <= 11) && (value >= 9) ){
       time = value + ':00AM';
    }else {
      time = value + ':00PM';
    }
    return time;
  }

}
