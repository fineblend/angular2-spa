import { Injectable, Inject } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {AuthService} from '../user/auth/auth.service';
import { TOASTR_TOKEN, Toastr } from './toastr';

@Injectable()
export class AgendaGuard implements CanActivate {
  constructor( private _auth: AuthService,
               @Inject(TOASTR_TOKEN) private _toastr: Toastr) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this._auth.authenticated === false) {
      this._toastr.info('You need to be logged in to do that');
    }
    // returns authentication status
    return this._auth.authenticated;
  }
}
