import { Injectable } from '@angular/core';

@Injectable()
export class AgendaService {

  constructor() {
  }

  getAppointments() {
    return [
      {
        'time': 9,
        'client': 'Jan C',
        'service': 'Massage',
        'comments': 'First Visit',
        'price': 50.0
      },
      {
        'time': 10,
        'client': 'Maria Jan',
        'service': 'Massage',
        'comments': 'First Visit',
        'price': 50.0
      },
      {
        'time': 11,
        'client': 'Dhalia G',
        'service': 'Massage',
        'comments': 'First Visit',
        'price': 50.0
      },
      {
        'time': 12,
        'client': 'Nata Bern',
        'service': 'Massage',
        'comments': 'First Visit',
        'price': 50.0
      },
      {
        'time': 1,
        'client': 'Peggy Mac',
        'service': 'Massage',
        'comments': 'First Visit',
        'price': 50.0
      },
      {
        'time': 2,
        'client': 'Lorna Al',
        'service': 'Massage',
        'comments': 'First Visit',
        'price': 50.0
      },
      {
        'time': 3,
        'client': 'Chi Su',
        'service': 'Massage',
        'comments': 'First Visit',
        'price': 50.0
      },
      {
        'time': 4,
        'client': 'Jan Sung',
        'service': 'Massage',
        'comments': 'First Visit',
        'price': 50.0
      },
      {
        'time': 5,
        'client': 'Mindy May',
        'service': 'Massage',
        'comments': 'First Visit',
        'price': 50.0
      },
      {
        'time': 6,
        'client': 'Beth Sue',
        'service': 'Massage',
        'comments': 'First Visit',
        'price': 50.0
      },
      {
        'time': 7,
        'client': 'Julie Brown',
        'service': 'Massage',
        'comments': 'First Visit',
        'price': 50.0
      },
      {
        'time': 8,
        'client': 'Julie May',
        'service': 'Massage',
        'comments': 'First Visit',
        'price': 50.0
      }
    ]
  }
}
