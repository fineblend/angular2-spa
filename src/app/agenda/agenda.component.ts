import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

import { IAppointment } from './../appointment/appointment.interface';
// import { AgendaService } from './../services/agenda.service';


@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {
  showEntry = false;
  pageTitle = 'Spa Agenda';
  message= 'This page shows the appointments for the day';
  page: any[] = [
    {
      'time': 9,
      'appointments' : [
        {'client': 'Jan C', 'service': 'Massage', 'price': 20.0, 'comments' : 'first visit'}
      ]
    },
    {
      'time': 10,
      'appointments' : [
        {'client': 'Maria Jan', 'service': 'Massage', 'price': 55.0, 'comments': 'First Visit'}
      ]
    },
    {
      'time': 11,
      'appointments' : [
        {'client': 'Maria Jan', 'service': 'Massage', 'price': 50.0, 'comments': 'First Visit'},
        {'client': 'Oli Jan', 'service': 'Massage', 'price': 22.0, 'comments': 'First Visit'}
      ]
    },
    {
      'time': 12,
      'appointments' : [
        {'client': 'Maria Jan', 'service': 'Massage', 'price': 50.0, 'comments': 'First Visit'},
        {'client': 'Oli Jan', 'service': 'Massage', 'price': 22.0, 'comments': 'First Visit'}
      ]
    },
    {
      'time': 1,
      'appointments' : [
        {'client': 'Maria Jan', 'service': 'Massage', 'price': 50.0, 'comments': 'First Visit'},
        {'client': 'Janice', 'service': 'Pedicure', 'price': 10.0, 'comments': 'First Visit'}
      ]
    },

  ];
  constructor() {};
  // splices an entry from the appointments object;
  removeEntry(entry: number, index: number): void {
    this.page[entry].appointments.splice(index, 1);
  }
  showEntryForm(): void {
    this.showEntry = true;
  }

  ngOnInit() {
  }

}
