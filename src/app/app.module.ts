import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AgendaComponent } from './agenda/agenda.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './user/login/login.component';

// angular material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MdButtonModule, MdCardModule, MdMenuModule, MdIconModule } from '@angular/material';
import 'hammerjs';

import { AgendaService } from './agenda/agenda.service';
import {AuthService} from './user/auth/auth.service';
import { TimePipe } from './common/time.pipe';
import { TOASTR_TOKEN, Toastr} from './common/toastr';
import { AppointmentComponent } from './appointment/appointment.component';
import {AgendaGuard} from './common/agenda.guard';

export let toastr: Toastr = window['toastr'];

@NgModule({
  declarations: [
    AppComponent,
    AgendaComponent,
    HomeComponent,
    TimePipe,
    LoginComponent,
    AppointmentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MdButtonModule,
    MdCardModule,
    MdMenuModule,
    MdIconModule,
    RouterModule.forRoot([
      { path: 'home', component : HomeComponent},
      { path: 'agenda', component : AgendaComponent, canActivate: [AgendaGuard]},
      {path : '', redirectTo : 'home', pathMatch : 'full'},
      {path : '**', redirectTo: 'home', pathMatch: 'full'}
    ])
  ],
  providers: [
    AuthService,
    { provide: TOASTR_TOKEN, useValue: toastr },
    AgendaGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
