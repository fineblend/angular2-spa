import { Component } from '@angular/core';
import { AuthService } from './user/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  footer = 'Developed by Kayode Anthony';
  pageTitle = 'Spa Diary';
  constructor(private auth: AuthService) {};
}
