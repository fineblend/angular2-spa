export interface IAppointment {
  time: number;
  client: string;
  service: string;
  comments: string;
  price: number;
}
