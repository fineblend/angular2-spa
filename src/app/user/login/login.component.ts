import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { TOASTR_TOKEN, Toastr } from '../../common/toastr';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor( private _authService: AuthService, private _router: Router,
    @Inject(TOASTR_TOKEN) private _toastr: Toastr) {
  }
  ngOnInit() {
  }
  login(formValues) {
    this._authService.loginUser(formValues.userName, formValues.password);
    this._toastr.success('Logged In');
    this._router.navigate(['agenda'], response => console.log(response));
  }
}
