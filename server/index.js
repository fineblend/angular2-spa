const path       = require('path');
const express    = require('express');
const mongoose   = require('mongoose');
const helmet     = require('helmet');
const bodyParser = require('body-parser');
const morgan     = require('morgan');
const bluebird   = require('bluebird');

const config = require('./config');
const routes = require('./routes');

const app  = express();

const baseUrl = path.normalize(__dirname + '/../dist');

mongoose.Promise = bluebird;
mongoose.connect(config.mongo.url);

app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('tiny'));

app.use(express.static(baseUrl));

app.use('/api', routes);

app.get('*', (req, res)=>{
  res.sendFile(path.join(baseUrl, index.html));
});

app.listen(config.server.port, () => {
  console.log(`Magic happens on port ${config.server.port}`);
});



module.exports = app;
