const path = require('path');
const Router = require('express').Router;
const router = new Router();

const agent = require('./model/agent/router');
const client = require('./model/client/router');
const item = require('./model/item/router');

const baseUrl = path.normalize(__dirname + '/../src');

router.use('/agent', agent);
router.use('/client', client);
router.use('/item', item);

// router.route('/*').get((req, res) => {
//   res.sendFile(path.join(baseUrl, 'index.html'));
// });

module.exports = router;
