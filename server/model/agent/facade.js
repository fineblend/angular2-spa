const Facade = require('../../lib/facade');
const agentSchema = require('./schema');

class AgentFacade extends Facade {}

module.exports = new AgentFacade(agentSchema);
