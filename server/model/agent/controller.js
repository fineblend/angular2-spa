const Controller = require('../../lib/controller');
const agentFacade = require('./facade');

class AgentController extends Controller {}

module.exports = new AgentController(agentFacade);
