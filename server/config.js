const config = {
  environment: process.env.NODE_ENV || 'dev',
  server: {
    port: process.env.PORT || 8085
  },
  mongo: {
    url: process.env.MONGO_DB_URI || 'mongodb://localhost/agenda'
  },
  secret:'secret'
};

module.exports = config;
